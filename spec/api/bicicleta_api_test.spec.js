var Bicicleta = require('../../models/bicicleta');
var request = require('request');
var server = require('../../bin/www');
var mongoose = require('mongoose');

var base_url = 'http://localhost:3000/api/bicicletas';

describe('Bicicleta API', () => {
    beforeAll(function (done) {
        var mongoDB = 'mongodb://localhost/testdb';
        mongoose.connect(mongoDB, { useNewUrlParser: true });

        const db = mongoose.connection;
        db.on('error', console.error.bind(console, 'connection error'));
        db.once('open', () => {
            console.log('We are connected to test database!');
            done();
        });
    });

    afterAll(function (done) {
        Bicicleta.deleteMany({}, function (err, success) {
            if (err) console.log(err);
            done();
        });
    });

    describe('GET BICICLETAS /', () => {
        it('Status 200', (done) => {
            request.get(base_url, (error, response, body) => {
                var result = JSON.parse(body);
                expect(response.statusCode).toBe(200);
                expect(result.bicicletas.length).toBe(0);
                done();
            });
        });
    });

    describe('POST BICICLETAS /create', () => {
        it('STATUS 200', (done) => {
            var headers = { 'content-type': 'application/json' };
            var aBici = '{ "code": 10, "color": "rojo", "modelo": "urbana", "lat": -34, "lng": -54 }';
            request.post({
                headers: headers,
                url: base_url + '/create',
                body: aBici
            }, (error, response, body) => {
                expect(response.statusCode).toBe(200);
                var bici = JSON.parse(body).bicicleta;
                expect(bici.color).toBe('rojo');
                expect(bici.ubicacion[0]).toBe(-34);
                expect(bici.ubicacion[1]).toBe(-54);
                done();
            });
        });
    });

    describe('DELETE BICICLETAS /delete', () => {
        it('STATUS 200', (done) => {
            var headers = { 'content-type': 'application/json' };
            request.get(base_url, (error, response, body) => {
                var result = JSON.parse(body);
                expect(result.bicicletas.length).toBe(1);

                request.delete({
                    headers: headers,
                    url: base_url + '/delete',
                    body: '{ "code": 10 }'
                }, (error, response, body) => {
                    expect(response.statusCode).toBe(204);

                    request.get(base_url, (error, response, body) => {
                        var result = JSON.parse(body);
                        expect(response.statusCode).toBe(200);
                        expect(result.bicicletas.length).toBe(0);
                        done();
                    });
                    done();
                });
            });
        });
    });
});