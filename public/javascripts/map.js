var mymap = L.map('main_map').setView([51.505, -0.09], 9);

L.tileLayer('https://api.mapbox.com/styles/v1/{id}/tiles/{z}/{x}/{y}?access_token=pk.eyJ1IjoiZGllZ29yZXllcyIsImEiOiJja2NxazB1bXUwaG0zMnFveXNzNmRtbHI0In0.YzhzJ4Dxgtfry-c5LXqgmQ', {
    attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors, <a href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
    maxZoom: 18,
    id: 'mapbox/streets-v11',
    tileSize: 512,
    zoomOffset: -1,
    accessToken: 'your.mapbox.access.token'
}).addTo(mymap);

$.ajax({
    dataType: 'json',
    url: 'api/bicicletas',
    success: (result) => {
        result.bicicletas.forEach(bici => {
            L.marker(bici.ubicacion, { title: bici.id }).addTo(mymap);
        });
    }
});